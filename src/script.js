import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import testVertexShader from './shaders/test/vertex.glsl'
import testFragmentShader from './shaders/test/fragment.glsl'
import gsap from "gsap";




// Debug
const gui = new dat.GUI()

// Loader
const textureLoader = new THREE.TextureLoader();

const texture = textureLoader.load('./assets/architecture.jpg');
const rewTexture = textureLoader.load('./assets/REW.png');

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Size
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
const scene = new THREE.Scene()

// Mesh
const planeGeometry = new THREE.PlaneGeometry(1, window.innerHeight / window.innerWidth, 100, 100);
const planeMaterial = new THREE.ShaderMaterial({

    vertexShader: testVertexShader,
    fragmentShader: testFragmentShader,
    side: THREE.DoubleSide,
    transparent: true,

    uniforms: {

        uTime: { value: 0 },
        uTexture: { value: texture },
        uRewTexture: { value: rewTexture },
        uGapDeformation: { value: 0.08 },
        uDeformationPosition: { value: 0 },
        uGapXPosition: { value: 0.011 },
        uIteration: { value: 0 },
        uGapNoise: { value: 0.04 },
        uGapNoisePosition: { value: 0 },
        uGapNoiseFrequencyX: { value: 3.6 },
        uGapNoiseFrequencyY: { value: 38 },
        uGapNoiseSpeed: { value: 6 },
        uGapSizzle: { value: 50 },
        uCanalRGBGap: { value: 0.001 },
        uRewSize: { value: 8 }

    }
});


// Bruit de rembobinage à ajouter
// ajouter un bruit général
// ajouter le mot REW >>
// ajout décalage color
gui.add(planeMaterial.uniforms.uGapDeformation, "value").min(0).max(0.6).name("Gap deformation")
gui.add(planeMaterial.uniforms.uDeformationPosition, "value").min(0).max(1).name("Deformation position")
gui.add(planeMaterial.uniforms.uGapXPosition, "value").min(0).max(0.02).name("Gap deformation X")
gui.add(planeMaterial.uniforms.uIteration, "value").min(0).max(20).name("Iteration")
gui.add(planeMaterial.uniforms.uGapNoise, "value").min(0).max(0.2).name("Gap of noise")
gui.add(planeMaterial.uniforms.uGapNoisePosition, "value").min(0).max(1).name("Gap of noise position")
gui.add(planeMaterial.uniforms.uGapNoiseFrequencyX, "value").min(0).max(100).name("Gap of noise Frequency X")
gui.add(planeMaterial.uniforms.uGapNoiseFrequencyY, "value").min(0).max(100).name("Gap of noise Frequency Y ")
gui.add(planeMaterial.uniforms.uGapNoiseSpeed, "value").min(0).max(10).name("Gap of noise speed")
gui.add(planeMaterial.uniforms.uGapSizzle, "value").min(25).max(100).name("Gap of noise speed")
gui.add(planeMaterial.uniforms.uCanalRGBGap, "value").min(0).max(0.0022).name("Gap RGB Color")
gui.add(planeMaterial.uniforms.uRewSize, "value").min(5).max(40).name("New size")
let ite = 50
var add = {

    add: () => {
        gsap.to(planeMaterial.uniforms.uIteration, { value: ite, duration: 4 });
        var audio = new Audio('./assets/sounds/sound.mp3');
        audio.play();
        ite += 50;
    }
}
gui.add(add, 'add');

const plane = new THREE.Mesh(planeGeometry, planeMaterial);

// plane.rotation.x = Math.PI / 2

scene.add(plane)




/**
 * Camera
 */
let camera = new THREE.PerspectiveCamera()

// Base camera
camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.01, 1000)
camera.position.set(0, 0, -0.5)
scene.add(camera)


window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})



// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true
controls.enabled = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})

renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

let clock = new THREE.Clock();

const tick = () => {

    planeMaterial.uniforms.uTime.value = clock.getElapsedTime();

    // Update controls
    controls.update()

    renderer.render(scene, camera)

    window.requestAnimationFrame(tick)

}

tick()