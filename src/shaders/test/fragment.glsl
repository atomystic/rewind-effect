// Variables

varying vec2 vUv;
uniform float uTime;
uniform sampler2D uTexture;
uniform sampler2D uRewTexture;
uniform float uGapDeformation;
uniform float uDeformationPosition;
uniform float uGapXPosition;
uniform float uIteration;
uniform float uGapNoise;
uniform float uGapNoisePosition;
varying float vNoiseBandColor;
uniform float uGapNoiseSpeed;
uniform float uCanalRGBGap;
uniform float uRewSize;





float moreDeformationInBorderOfBand(float gapBand) {

     float borderFactor;

     borderFactor=1.0-abs(gapBand)/(uGapDeformation);

     borderFactor =pow(borderFactor,0.4);

     return borderFactor;

}

float getDeformationFactor(){

     // définition de la position des bordures de la bande. La position est en modulo pour avoir plusieurs itération
     float deformationPositionMod =uDeformationPosition + mod(uIteration,1.0);

     // écart  entre la position y et la position de la déformation
     float gapBand =vUv.y-deformationPositionMod;

     // bande : Positionnement et largeur de la bande
     float strength = step(uGapDeformation,abs(gapBand));
 
     // facteur de déformation sur une bande
     float deformFactor = 1.0-strength;

     if(abs(gapBand)<uGapDeformation){

          // Déformation moins forte aux extrémités hautes et basses de la bande
          deformFactor*= moreDeformationInBorderOfBand(gapBand);

     }

     return deformFactor;
     //https://www.youtube.com/watch?v=zByO2TmM1WU&t=409s mettre des valeurs absolue==> la déformation se fait par palier ==> méthode floor

} 

float getNoiseBandFactor(){

     //Ciblage d'une seule bande de hauteur 2 fois le gap
     float gapNoisePositionMod = uGapNoisePosition+ mod(uIteration,uGapNoiseSpeed)/uGapNoiseSpeed;
     
     //Ciblage d'une seule bande (Plusieurs dans l'avenir?, adoucir la bande aux extrémités?)
     float noiseBandFactor =1.0-step(uGapNoise,abs(vUv.y-gapNoisePositionMod));

     // ce qui sont hors de la bande donnera zero==> pas de changement de couleur car ajout de zero à chaque canal rgb
     noiseBandFactor*= vNoiseBandColor;

     return noiseBandFactor;

}

vec2 getGapCanalColor(){

     float color2 = 1.0*uCanalRGBGap;
     float color3 = 2.0*uCanalRGBGap;

     return vec2(color2,color3);
}

float random2d(vec2 uv){

     return fract(sin(dot(uv.xy,vec2(12.9898*((sin(uTime)/2.0)+0.5),78.233*((sin(uTime)/2.0)+0.5))))*43758.5453);
}



void main(){
     /*
     * Facteur et Additionneur
     */

     // Déclage vers la droite 
     float deformFactor = getDeformationFactor();

     // Bande de bruit
     float noiseBandFactor = getNoiseBandFactor();

     // Décalage RGB
     vec2 canalColor = getGapCanalColor();

     // mot new
     vec4 rewWord = texture2D(uRewTexture,vec2((vUv.x-0.82+deformFactor*uGapXPosition)*uRewSize,(vUv.y-0.85)*2.2*uRewSize));

     // grain général
     float grain = random2d((vUv/5.0)-0.5)*0.1;

     


     
     /*
     * Construction de la couleur final
     */


     //décalage en x et décalage des couches rgb, mot new
     vec4 color1 =  texture2D(uTexture,vec2(vUv.x+deformFactor*uGapXPosition+canalColor.x,vUv.y));
     vec4 color2 =  texture2D(uTexture,vec2(vUv.x+deformFactor*uGapXPosition+canalColor.y,vUv.y));
     vec4 color3 =  texture2D(uTexture,vec2(vUv.x+deformFactor*uGapXPosition,vUv.y));
     vec4 color =vec4(color1.r,color2.g,color3.b,1.0);

     // mot rew
     color =clamp(color+rewWord,0.,1.);

     // bande de bruit
     color+= clamp(vec4(noiseBandFactor,noiseBandFactor,noiseBandFactor,0.0),0.,1.);

     //
     color+= grain;
     




     gl_FragColor = vec4(color);

}